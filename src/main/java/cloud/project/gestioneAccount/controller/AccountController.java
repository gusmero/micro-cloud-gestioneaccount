package cloud.project.gestioneAccount.controller;

import java.util.List;
import java.util.ArrayList;
import java.util.Optional;
import java.util.Map;
import java.util.HashMap;

//import java.lang.Integer;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RestController;

import cloud.project.gestioneAccount.domain.Account;
import cloud.project.gestioneAccount.service.AccountService;

import java.io.IOException;

import java.net.URISyntaxException;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;

import org.springframework.ui.Model;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PatchMapping;

import org.json.JSONObject;
import org.json.JSONArray;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;


@RestController
public class AccountController {
    
    @Autowired
    private AccountService accountService;
    
    private ObjectMapper objectMapper = new ObjectMapper();
    
    
    @GetMapping("/account/all")
    public ResponseEntity<?> getAllAccounts(HttpServletRequest request, HttpServletResponse response) {
        List<Account> accounts = accountService.getAllAccounts();
        JSONArray jsonAccounts = new JSONArray();
        String be = "[";
        String end = "]";
        String f = "";
        try {
            for (Account account: accounts) {
                f = f + account.toString() + ", ";
                //JSONObject jAccount = new JSONObject(objectMapper.writeValueAsString(account));
                //jsonAccounts.put(jAccount);
            }
            //System.out.println(jsonAccounts);
            f = f.substring(0, f.length()-2);
            f = be + f + end;
            System.out.println(f);
            Map<String, Object> jsonResponse = new HashMap<String, Object>();
            jsonResponse.put("response", f);
            System.out.println(jsonResponse);
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            return new ResponseEntity<>(jsonResponse, headers, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            Map<String, Object> jsonResponse = new HashMap<String, Object>();
            jsonResponse.put("response", "Errore nel parsing, lato servizio account.");
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            return new ResponseEntity<>(jsonResponse, headers, HttpStatus.OK);            
        }        
    }
	
	
	@PostMapping("/account/create")
	public ResponseEntity<?> createAccount(HttpServletRequest request, HttpServletResponse response, @RequestBody String accountJson) throws IOException {
	   // printo il json in entrata al controller
	   System.out.println(accountJson);
	   // converto il json in entrata in un oggetto Account
	   Account account = objectMapper.readValue(accountJson, Account.class);
	   System.out.println(account.toString());
	   // mi assicuro quantomeno che esista il campo email
	   if (account.getEmail().equalsIgnoreCase("")) {
	      Map<String, Object> jsonResponse = new HashMap<String, Object>();
	      jsonResponse.put("response", "Errore. Il campo email risulta vuoto.");
	      HttpHeaders headers = new HttpHeaders();
              headers.setContentType(MediaType.APPLICATION_JSON);
	      return new ResponseEntity<>(jsonResponse, headers, HttpStatus.OK);
	   }
	   if (accountService.saveAccount(account)) {
	      Map<String, Object> jsonResponse = new HashMap<String, Object>();
	      jsonResponse.put("response", "Account creato.");
	      HttpHeaders headers = new HttpHeaders();
              headers.setContentType(MediaType.APPLICATION_JSON);
	      return new ResponseEntity<>(jsonResponse, headers, HttpStatus.CREATED);
	   } else {
	      //JSONObject jsonResponse = new JSONObject("{ \"response\": \"Account già esistente.\" }");
	      Map<String, Object> jsonResponse = new HashMap<String, Object>();
	      jsonResponse.put("response", "Account già esistente.");
	      HttpHeaders headers = new HttpHeaders();
              headers.setContentType(MediaType.APPLICATION_JSON);
	      //System.out.println(jsonResponse);
	      //String json = objectMapper.writeValueAsString(jsonResponse);
	      return new ResponseEntity<>(jsonResponse, headers, HttpStatus.OK);
	   }
	}
	
	
	@GetMapping("/account/get")
	public ResponseEntity<?> getAccount(HttpServletRequest request, HttpServletResponse response, @RequestBody String emailJson) throws JsonProcessingException {
	    System.out.println(emailJson);
	    String email = objectMapper.readValue(emailJson, String.class);
	    Account account = accountService.getAccount(email);
	    //Optional<Account> account = accountService.getAccount(email).get;
	    if (account == null) {
	       Map<String, Object> jsonResponse = new HashMap<String, Object>();
	       jsonResponse.put("response", "Account non trovato.");
	       HttpHeaders headers = new HttpHeaders();
               headers.setContentType(MediaType.APPLICATION_JSON);
	       return new ResponseEntity<>(jsonResponse, headers, HttpStatus.NOT_FOUND);
	    } else {
	       Map<String, Object> jsonResponse = new HashMap<String, Object>();
	       jsonResponse.put("response", "Account trovato.");
	       HttpHeaders headers = new HttpHeaders();
               headers.setContentType(MediaType.APPLICATION_JSON);
	       return new ResponseEntity<>(jsonResponse, headers, HttpStatus.FOUND);
	    }
	}
		
	
	@PatchMapping("/account/update")
	public ResponseEntity<?> updateAccount(HttpServletRequest req, HttpServletResponse res, @RequestBody String jsonModifications) throws JsonProcessingException {
	   int telephoneNumber = 0; // set default
	   System.out.println(jsonModifications);
	   JSONObject jsonObject = new JSONObject(jsonModifications);
	   System.out.println(jsonObject);
	   try {
	      String email = jsonObject.getString("email");
	      String password = jsonObject.getString("password"); 
	      if (email == null || password == null) {
	         Map<String, Object> jsonResponse = new HashMap<String, Object>();
	         jsonResponse.put("response", "Errore. Alcuni campi risultano null.");
	         HttpHeaders headers = new HttpHeaders();
                 headers.setContentType(MediaType.APPLICATION_JSON);
	         return new ResponseEntity<>(jsonResponse, headers, HttpStatus.OK);
	      }
	      if (email.equalsIgnoreCase("") || password.equalsIgnoreCase("")) {
	         Map<String, Object> jsonResponse = new HashMap<String, Object>();
	         jsonResponse.put("response", "Errore. Alcuni campi risultano vuoti.");
	         HttpHeaders headers = new HttpHeaders();
                 headers.setContentType(MediaType.APPLICATION_JSON);
	         return new ResponseEntity<>(jsonResponse, headers, HttpStatus.OK);
	      }	      
	      if (!accountService.existsAccount(email)) {
	         Map<String, Object> jsonResponse = new HashMap<String, Object>();
	         jsonResponse.put("response", "Errore. L'account non è stato trovato.");
	         HttpHeaders headers = new HttpHeaders();
                 headers.setContentType(MediaType.APPLICATION_JSON);
	         return new ResponseEntity<>(jsonResponse, headers, HttpStatus.OK);
	      }
	      String firstName = jsonObject.getString("firstName");
	      String lastName = jsonObject.getString("lastName");
	      try {
	          telephoneNumber = Integer.parseInt(jsonObject.getString("telephoneNumber"));
	      } catch (Exception e) {
	          e.printStackTrace();
	      }	      
	      if (accountService.updateAccount(firstName, lastName, email, password, telephoneNumber)) {
	         Map<String, Object> jsonResponse = new HashMap<String, Object>();
	         jsonResponse.put("response", "Account aggiornato correttamente.");
	         HttpHeaders headers = new HttpHeaders();
                 headers.setContentType(MediaType.APPLICATION_JSON);
	         return new ResponseEntity<>(jsonResponse, headers, HttpStatus.OK);
	      } else {
	         Map<String, Object> jsonResponse = new HashMap<String, Object>();
	         jsonResponse.put("response", "Errore nell'aggiornamento dell'account.");
	         HttpHeaders headers = new HttpHeaders();
                 headers.setContentType(MediaType.APPLICATION_JSON);
	         return new ResponseEntity<>(jsonResponse, headers, HttpStatus.OK);
	      }
	   } catch (Exception e) {
	       e.printStackTrace();
	       Map<String, Object> jsonResponse = new HashMap<String, Object>();
	       jsonResponse.put("response", "Internal Server Error");
	       HttpHeaders headers = new HttpHeaders();
               headers.setContentType(MediaType.APPLICATION_JSON);
	       return new ResponseEntity<>(jsonResponse, headers, HttpStatus.INTERNAL_SERVER_ERROR);
	   }	   
	}
	
	
	@DeleteMapping("/account/delete")
	public ResponseEntity<?> deleteAccount(HttpServletRequest request, HttpServletResponse response, @RequestBody String emailJson) throws JsonProcessingException {
	    System.out.println(emailJson);
	    JSONObject jsonEmail = new JSONObject(emailJson);
	    String email = jsonEmail.getString("email");
	    if (accountService.deleteAccount(email)) {
	       Map<String, Object> jsonResponse = new HashMap<String, Object>();
	         jsonResponse.put("response", "Account eliminato.");
	         HttpHeaders headers = new HttpHeaders();
                 headers.setContentType(MediaType.APPLICATION_JSON);
	         return new ResponseEntity<>(jsonResponse, headers, HttpStatus.OK);
	    } else {
	       Map<String, Object> jsonResponse = new HashMap<String, Object>();
	         jsonResponse.put("response", "Errore. L'account non è stato trovato.");
	         HttpHeaders headers = new HttpHeaders();
                 headers.setContentType(MediaType.APPLICATION_JSON);
	         return new ResponseEntity<>(jsonResponse, headers, HttpStatus.OK);
	    }
	}


}



