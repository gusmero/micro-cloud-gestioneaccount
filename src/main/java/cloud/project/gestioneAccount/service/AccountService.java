package cloud.project.gestioneAccount.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import cloud.project.gestioneAccount.domain.Account;
import cloud.project.gestioneAccount.repository.AccountRepository;


@Service
public class AccountService {

	@Autowired
	private  AccountRepository accountRepository;
	
	@Autowired
	private BCryptPasswordEncoder passwordEncoder;

	@Transactional
	public boolean saveAccount(String firstName, String lastName, String email, String password, int telephoneNumber) throws IOException {
	    if (accountRepository.existsByEmail(email)) {
	        return false;
	    } else {
	        Account account = new Account(firstName, lastName, email, passwordEncoder.encode(password), telephoneNumber);
	        accountRepository.save(account);
	        return true;
	    }	   
	}
	
	@Transactional
	public boolean saveAccount(Account account) throws IOException {
	    if (accountRepository.existsByEmail(account.getEmail()) ||
			account.getLastName().equalsIgnoreCase("") || account.getLastName() == null ||
			account.getPassword().equalsIgnoreCase("") || account.getPassword() == null) {
	        return false;
	    } else {
			account.setPassword(passwordEncoder.encode(account.getPassword()));
	        accountRepository.save(account);
	        return true;
	    }	   
	}

	public Account getAccount(String email) {
	   return accountRepository.findByEmail(email).get();
	}
	
	public List<Account> getAllAccounts() {
	    return accountRepository.findAll();
	}
	
	public boolean existsAccount(String email) {
	   return accountRepository.existsByEmail(email);
	}
	
	@Transactional
	public boolean updateAccount(String firstName, String lastName, String email, String password, int telephoneNumber) {
	   Account accountToModify = accountRepository.findByEmail(email).get();
	   if (accountToModify == null) return false;
	   else {
	      if (firstName.equals("")) ;
	      else accountToModify.setFirstName(firstName);
	      if (lastName.equals("")) ;
	      else accountToModify.setLastName(lastName);
	      accountToModify.setPassword(passwordEncoder.encode(password));
	      accountToModify.setTelephoneNumber(telephoneNumber);
	      accountRepository.save(accountToModify);
	      return true;
	   }	   
	}

    @Transactional
    public boolean deleteAccount(String email) {
        if (accountRepository.deleteByEmail(email) instanceof Long) return true;
        else return false;
    }
	
	

}
