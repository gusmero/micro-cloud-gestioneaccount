package cloud.project.gestioneAccount;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class GestioneAccountApplication {

	public static void main(String[] args) {
		// Tell Boot to look for registration-server.yml
	    System.setProperty("spring.config.name", "registration-client");
		SpringApplication.run(GestioneAccountApplication.class, args);
	}

	
}
