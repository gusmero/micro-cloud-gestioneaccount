package cloud.project.gestioneAccount.domain;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
// import javax.persistence.EnumType;
// import javax.persistence.Enumerated;
// import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
// import javax.persistence.JoinColumn;
// import javax.persistence.ManyToOne;
// import javax.persistence.OneToMany;
import javax.persistence.Table;
// import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;


@Entity
@Table(name = "account")
public class Account {

   @Id
   @GeneratedValue(strategy = GenerationType.AUTO)
   @Column(name="id")
   private Long id;

   @Column(name="firstname")
   private String firstName;
   
   @NotNull
   @Column(name="lastname")
   private String lastName;
   
   @NotNull
   @Column(name="email", unique = true)
   private String email;
   
   @NotNull
   @Column(name="password")
   private String password;
   
   @Column(name="telephonenumber")
   private int telephoneNumber;
   
   public Account() {}
   
   public Account(String firstName, String lastName, String email, String password, int telephoneNumber) {
      setFirstName(firstName);
      setLastName(lastName);
      setEmail(email);
      setPassword(password);
      setTelephoneNumber(telephoneNumber);
   }
   
   public Long getId() {
      return id;
   }

   public void setFirstName(String firstName) {
      this.firstName = firstName;
   }

   public String getFirstName() {
      return firstName;
   }

   public void setLastName(String lastName) {
      this.lastName = lastName;
   }

   public String getLastName() {
      return lastName;
   }

   public void setEmail(String email) {
      this.email = email;
   }

   public String getEmail() {
      return email;
   }

   public void setPassword(String password) {
      this.password = password;
   }

   public String getPassword() {
      return password;
   }

   public void setTelephoneNumber(int telephoneNumber) {
      this.telephoneNumber = telephoneNumber;
   }
   
   public int getTelephoneNumber() {
      return telephoneNumber;
   }

   @Override
   public String toString(){
      String start = "{ ";
      String end = " }";
      String id = " \"id\": \"" + getId() + "\",";
      String fn = "\"firstName\": \"" + getFirstName() + "\",";
      String ln = "\"lastName\": \"" + getLastName() + "\",";
      String e = "\"email\": \"" + getEmail() + "\",";
      String p = "\"password\": \"" + getPassword() + "\",";
      String t = "\"telephoneNumber\": \"" + getTelephoneNumber() + "\"";
      return start + id + fn + ln + e + p + t + end;
   }
   

}
