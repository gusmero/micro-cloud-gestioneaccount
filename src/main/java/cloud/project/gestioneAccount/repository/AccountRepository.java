package cloud.project.gestioneAccount.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import cloud.project.gestioneAccount.domain.Account;

public interface AccountRepository extends JpaRepository<Account, Long> {

   Optional<Account> findByEmail(String email);
   Long deleteByEmail(String email);
   boolean existsByEmail(String email);

}
