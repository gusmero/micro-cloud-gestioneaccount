FROM openjdk:11-jre

COPY  /target/gestioneAccount-0.0.1-SNAPSHOT.jar ./


EXPOSE 2222
CMD ["java", "-jar", "gestioneAccount-0.0.1-SNAPSHOT.jar"]

